function getDayOfMonth (dateStr) {
  const inputDate = new Date(dateStr);
  return inputDate.getDate();
}

function getDayOfWeek (dateStr) {
  const inputDate = new Date(dateStr);
  return inputDate.getDay();
}

function getYear (dateStr) {
  const inputDate = new Date(dateStr);
  return inputDate.getFullYear();
}

function getMonth (dateStr) {
  const inputDate = new Date(dateStr);
  return inputDate.getMonth();
}

function getMilliseconds (dateStr) {
  const inputDate = new Date(dateStr);
  return inputDate.getTime();
}

export { getDayOfMonth, getDayOfWeek, getYear, getMonth, getMilliseconds };
