function flatArray (arr) {
  return arr.flat(1);
}

function aggregateArray (arr) {
  return arr.map(x => [x * 2]);
}

function getEnumerableProperties (obj) {
  return Object.keys(obj);
}

function removeDuplicateItems (arr) {
  return [...new Set(arr)];
}

function removeDuplicateChar (str) {
  return [...new Set(str.split(''))].join('');
}

function addItemToSet (set, item) {
  set.add(item);
  return set;
}

function removeItemToSet (set, item) {
  set.delete(item);
  return set;
}

function countItems (arr) {
  const resultMap = new Map();
  [...new Set(arr)].map(x => resultMap.set(x, arr.filter(y => y === x).length));
  return resultMap;
}

export {
  flatArray,
  aggregateArray,
  getEnumerableProperties,
  removeDuplicateItems,
  removeDuplicateChar,
  addItemToSet,
  removeItemToSet,
  countItems
};
