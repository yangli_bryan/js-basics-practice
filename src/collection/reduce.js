function getMaxNumber (collction) {
  return collction.reduce((maximumValue, currentValue) => maximumValue < currentValue ? currentValue : maximumValue);
}

function isSameCollection (collction1, collction2) {
  const sumFunction = (accumulator, currentValue) => accumulator + currentValue;
  const productFunction = (accumulator, currentValue) => accumulator * currentValue;
  const sameSum = collction1.reduce(sumFunction, 0) === collction2.reduce(sumFunction, 0);
  const sameProduct = collction1.reduce(productFunction, 1) === collction2.reduce(productFunction, 1);
  return sameSum && sameProduct;
}

function sum (collction) {
  return collction.reduce((accumulator, currentValue) => accumulator + currentValue, 0)
}

function computeAverage (collction) {
  return collction.reduce((accumulator, currentValue) => accumulator + currentValue, 0) / collction.length;
}

function lastEven (collction) {
  return collction.reduce((previousValue, currentValue) => currentValue % 2 === 0 ? currentValue : previousValue);
}

export { getMaxNumber, isSameCollection, computeAverage, sum, lastEven };
