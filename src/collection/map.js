function doubleItem (collection) {
  return collection.map(x => x * 2);
}

function doubleEvenItem (collection) {
  return collection.filter(x => x % 2 === 0).map(x => x * 2);
}

function covertToCharArray (collection) {
  return collection.map(x => String.fromCharCode(96 + x));
}

function getOneClassScoreByASC (collection) {
  return collection.filter(x => x.class === 1).map(x => x.score).sort((a, b) => a - b);
}

export { doubleItem, doubleEvenItem, covertToCharArray, getOneClassScoreByASC };
