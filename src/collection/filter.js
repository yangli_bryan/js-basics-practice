function getAllEvens (collection) {
  return collection.filter(element => element % 2 === 0);
}

function getAllIncrementEvens (start, end) {
  return [...Array(end - start + 1).keys()].map(x => x + start).filter(x => x % 2 === 0);
}

function getIntersectionOfcollections (collection1, collection2) {
  return collection1.filter(element => collection2.includes(element));
}

function getUnionOfcollections (collection1, collection2) {
  return collection1.concat(collection2.filter(element => !collection1.includes(element)));
}

function countItems (collection) {
  const countResult = {};
  const elementSet = [...new Set(collection)];
  for (let i = 0; i < elementSet.length; i++) {
    countResult[elementSet[i]] = collection.filter(item => item === elementSet[i]).length;
  }
  return countResult;
}

export {
  getAllEvens,
  getAllIncrementEvens,
  getIntersectionOfcollections,
  getUnionOfcollections,
  countItems
};
